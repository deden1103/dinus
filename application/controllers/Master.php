<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Master extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Master_Model');

    }

    public function provinsi_get()
    {
        $provinces = $this->Master_Model->provinces();
        $response = array(
            'success' => true,
            'data' => $provinces,
           
        );
        $this->response($response, 200);
    }

    public function kota_get()
    {
        $provid=$this->get('provinsi_id');
        $provinces = $this->Master_Model->city($provid);
        $response = array(
            'success' => true,
            'data' => $provinces,
           
        );
        $this->response($response, 200);
    }


    public function kecamatan_get()
    {
        $kotaid=$this->get('kota_id');
        $data = $this->Master_Model->kecamatan($kotaid);
        $response = array(
            'success' => true,
            'data' => $data,
           
        );
        $this->response($response, 200);
    }

    public function kelurahan_get()
    {
        $kecamatanid=$this->get('kecamatan_id');
        $data = $this->Master_Model->kelurahan($kecamatanid);
        $response = array(
            'success' => true,
            'data' => $data,
           
        );
        $this->response($response, 200);
    }

    public function index_get()
    {
        $dataDetail = array();
        $this->db->select('*');
        $this->db->from('about');
        $query = $this->db->get()->row();

        $response = array(

            'detail' => $query,
            'success' => true,
        );
        $this->response($response, 200);
    }

   

}
