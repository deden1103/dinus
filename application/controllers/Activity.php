<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Activity extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Activity_Model']);
    }

    public function index_get()
    {
        $page = 1;
        $searchtext = '';
        $type = '';
        if ($this->get('type')) {
            $type = intval($this->get('type'));
        }

        if ($this->get('p')) {
            $page = intval($this->get('p'));
        }

        if ($this->get('s')) {
            $searchtext = security_single_post($this->get('s'));
        }

        $rows = 10;
        $offset = ($page - 1) * $rows;
        $listNews = $this->Activity_Model->list_activity(
            $type,
            $rows,
            $offset,
            $searchtext
        );
        $response = [
            'success' => true,
            'page' => $page,
            'next_page' => $page + 1,
            'data' => $listNews,
        ];
        $this->response($response, 200);
    }
}
