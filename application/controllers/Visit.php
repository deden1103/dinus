<?php
defined('BASEPATH') or exit('No direct script access allowed');
// require APPPATH . '/libraries/REST_Controller.php';
class Visit extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Visit_Model']);
    }

    public function index_get()
    {
        $id = $this->get('id');
        // print_r($id);die;

        $visitData = $this->Visit_Model->detail($id);
        // if ($itenData) {
        $response = [
            // 'message' => 'Login Berhasil',
            'success' => true,
            'data' => $visitData,
        ];

        $this->response($response, 200);
    }
    public function index_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $arrayValidation = [
            'vstDate',
            'vstUserId',
            'vstOutletId',
            'vstReasonId',
            'vstCheckIn',
        ];

        foreach ($arrayValidation as $r) {
            $this->form_validation->set_rules($r, $r, 'trim|required');
        }
        $this->form_validation->set_message('required', '{field} harus diisi');

        if ($this->form_validation->run() == false) {
            $response = [
                'success' => false,
                'errors' => $this->form_validation->error_array(),
                'data' => [],
            ];
            $this->response($response, 200);
            exit();
        }


        if(!empty($post['id'])){
            $iten = explode("----",$post['vstDate']);
            $data = array(
                'vstUserId' => $post['vstUserId'],
                'vstOutletId' => $post['vstOutletId'],
                'vstReasonId' => $post['vstReasonId'],
                'vstItenId' => $iten[1],
                'vstWeek' => date("W", strtotime($iten[0])),
                'vstDate' => $iten[0],
                'vstCheckIn' => $post['vstCheckIn'],
                'vstCheckOut' => $post['vstCheckOut'],
                'vstUserUpdated' => $this->jwtData->id,
            );
            // $data['outId'] = $post['id'];
            // $data['outDcId'] = $post['dc_id'];
            // $data['outCityId'] = $post['city_id'];
            // $data['outProvinceId'] = $post['province_id'];
            // $data['outIdCust'] = $post['outlet_id'];
            // $data['outName'] = $post['name'];
            // $data['outAddress'] = $post['address'];
            // $data['outPhone'] = $post['phone'];
            // $data['outContactPerson'] = $post['contact_person'];
            // $data['outEmail'] = $post['email'];
            // $data['outPhoto'] = $post['photo'];
            // $data['outLatitude'] = $post['latitude'];
            // $data['outLongitude'] = $post['longitude'];
            // $data['outUpdated'] = date("Y-m-d H:i:s");
            // $data['outUserUpdated'] = $this->jwtData->id;//$post['user_id'];
            $visitData = $this->Visit_Model->update($data);
    
            $response = [
                'data' => $visitData,
                'message' => 'Data Berhasil di Ubah',
                'success' => true,
            ];
        } else {
            $iten = explode("----",$post['vstDate']);
            $data = array(
                'vstUserId' => $post['vstUserId'],
                'vstOutletId' => $post['vstOutletId'],
                'vstReasonId' => $post['vstReasonId'],
                'vstItenId' => $iten[1],
                'vstWeek' => date("W", strtotime($iten[0])),
                'vstDate' => $iten[0],
                'vstCheckIn' => $post['vstCheckIn'],
                'vstCheckOut' => $post['vstCheckOut'],
                'vstUserSaved' => $this->jwtData->id,
                'vstUserUpdated' => $this->jwtData->id,
            );
            // $data['outDcId'] = $post['dc_id'];
            // $data['outCityId'] = $post['city_id'];
            // $data['outProvinceId'] = $post['province_id'];
            // $data['outIdCust'] = $post['outlet_id'];
            // $data['outName'] = $post['name'];
            // $data['outAddress'] = $post['address'];
            // $data['outPhone'] = $post['phone'];
            // $data['outContactPerson'] = $post['contact_person'];
            // $data['outEmail'] = $post['email'];
            // $data['outPhoto'] = $post['photo'];
            // $data['outLatitude'] = $post['latitude'];
            // $data['outLongitude'] = $post['longitude'];
            // $data['outSaved'] = date("Y-m-d H:i:s");
            // $data['outUserSaved'] = $this->jwtData->id;//$post['user_id'];
            $visitData = $this->Visit_Model->add($data, $post['vstId']);
    
            $response = [
                'data' => $visitData,
                'message' => 'Data Berhasil di Tambah',
                'success' => true,
            ];
        }
        $this->response($response, 200);
    }
}