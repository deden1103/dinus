<?php
class Province_Model extends CI_Model
{

    function list($rows,$offset,$searchtext)
    {
        $sql = "SELECT * FROM provinces";

        if ($searchtext) {
            $sql .= " WHERE provName LIKE '%{$searchtext}%' ";
        }

        if ($offset) {
            $sql .= " LIMIT {$rows} OFFSET {$offset}";
        }

        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }
    }

}