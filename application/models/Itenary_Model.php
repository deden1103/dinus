<?php
class Itenary_Model extends CI_Model
{
    function detail($userid, $d = '', $m = '', $y = '')
    {
        $sql = 'SELECT * FROM itenary';
        $sql .= ' LEFT JOIN user ON itenary.itenUserId = user.userId';
        $sql .= ' LEFT JOIN outlet ON itenary.itenOutletId = outlet.outId';
        $sql .= " WHERE itenUserId = {$userid}";
        $sql .= " AND DAY(itenDate) = {$d}";
        $sql .= " AND MONTH(itenDate) = {$m}";
        $sql .= " AND YEAR(itenDate) = {$y}";
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }

        // $query = $this->db
        //     ->select('a.*, b.userUsername, c.outName')
        //     ->from('itenary a')
        //     ->join('user b', 'b.userId=a.itenUserId', 'LEFT' )
        //     ->join('outlet c', 'c.outId=a.itenOutletId', 'LEFT')
        //     ->order_by('a.itenSaved', 'DESC')
        //     ->where("a.itenStatus", 1)
        //     ->get();
        //     // print_r($this->db->last_query());
        // return $query->row();
    }
}
