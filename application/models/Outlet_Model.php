<?php
class Outlet_Model extends CI_Model
{
    function add_outlet($post, $jwtid)
    {
        $outDcId = security_single_post($post['outDcId']);
        $outCityId = security_single_post($post['outCityId']);
        $outProvinceId = security_single_post($post['outProvinceId']);
        $outIdCust = security_single_post($post['outIdCust']);
        $outName = security_single_post($post['outName']);
        $outAddress = security_single_post($post['outAddress']);
        $outPhone = security_single_post($post['outPhone']);
        $outContactPerson = security_single_post($post['outContactPerson']);
        $outEmail = security_single_post($post['outEmail']);
        $outLatitude = security_single_post($post['outLatitude']);
        $outLongitude = security_single_post($post['outLongitude']);
        $outUserSaved = security_single_post($post['outUserSaved']);
        $outUserUpdated = security_single_post($post['outUserUpdated']);

        $this->db->insert('outlet', [
            'outDcId' => $outDcId,
            'outCityId' => $outCityId,
            'outProvinceId' => $outProvinceId,
            'outIdCust' => $outIdCust,
            'outName' => $outName,
            'outAddress' => $outAddress,
            'outPhone' => $outPhone,
            'outContactPerson' => $outContactPerson,
            'outEmail' => $outEmail,
            'outPhoto' => $outLatitude,
            'outLatitude' => $outLongitude,
            'outLongitude' => $outLongitude,
            'outUserSaved' => $jwtid,
            'outUserUpdated' => $outUserUpdated,
        ]);

        $insertid = $this->db->insert_id();
        $data = [];
        if ($insertid) {
            $data = [
                'insert_id' => $this->db->insert_id(),
            ];
        }

        return $data;
    }

    function mylist($id, $d = '', $m = '', $y = '')
    {
      

        $sql =
            'SELECT outId,outDcId,outCityId,outIdCust,outName,outAddress,
            outPhone,outContactPerson,outEmail,outLatitude,outLongitude,c.cityName,p.provName FROM itenary';
        $sql .= ' LEFT JOIN user ON itenary.itenUserId = user.userId';
        $sql .= ' LEFT JOIN outlet ON itenary.itenOutletId = outlet.outId';
        $sql .= ' LEFT JOIN city c ON c.cityId = outlet.outCityId';
        $sql .= ' LEFT JOIN provinces p ON outlet.outProvinceId = p.provId';
        $sql .= " WHERE itenUserId = {$id}";
        $sql .= " AND DAY(itenDate) = {$d}";
        $sql .= " AND MONTH(itenDate) = {$m}";
        $sql .= " AND YEAR(itenDate) = {$y}";
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }

        // $sql = 'SELECT * FROM outlet';
        // $sql .= " WHERE outIdCust = {$id}";
        // if ($searchtext) {
        //     $sql .= " AND newsTitle LIKE '%{$searchtext}%' ";
        // }

        // if ($offset) {
        //     $sql .= " LIMIT {$limit} OFFSET {$offset}";
        // }
        // $query = $this->db->query($sql)->result_array();
        // return $query;
    }

    function all_list($id)
    {
        $sql =
        'SELECT DISTINCT outId,outDcId,outCityId,outIdCust,outName,
        outAddress,outPhone,outContactPerson,outEmail,outLatitude,
        outLongitude, a.dcCode, a.dcName,c.cityName,p.provName FROM itenary';
        $sql .= ' LEFT JOIN user ON itenary.itenUserId = user.userId';
        $sql .= ' LEFT JOIN outlet ON itenary.itenOutletId = outlet.outId';
        $sql .= ' LEFT JOIN distribution_center a ON a.dcId = outlet.outDcId';
        $sql .= ' LEFT JOIN city c ON c.cityId = outlet.outCityId';
        $sql .= ' LEFT JOIN provinces p ON outlet.outProvinceId = p.provId';
        $sql .= " WHERE itenUserId = {$id}";
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }
    }

    function detail_outlet($id)
    {
        $sql = 'SELECT * FROM outlet';
        $sql .= " WHERE outId = {$id}";
        $query = $this->db->query($sql)->result_array();
        foreach ($query as $key => $value) {
            $query[$key]['outPhoto'] = $this->additional->image_outlet(
                $id,
                $value['outPhoto']
            );
        }
        return $query;
    }

    function update_outlet($post, $jwtid)
    {
        // $outDcId = security_single_post($post['outDcId']);
        // $outCityId = security_single_post($post['outCityId']);
        // $outProvinceId = security_single_post($post['outProvinceId']);
        // $outIdCust = security_single_post($post['outIdCust']);
        // $outName = security_single_post($post['outName']);
        // $outAddress = security_single_post($post['outAddress']);
        // $outPhone = security_single_post($post['outPhone']);
        // $outContactPerson = security_single_post($post['outContactPerson']);
        // $outEmail = security_single_post($post['outEmail']);
        $outLatitude = security_single_post($post['outLatitude']);
        $outLongitude = security_single_post($post['outLongitude']);
        // $outUserSaved = security_single_post($post['outUserSaved']);
        // $outUserUpdated = security_single_post($post['outUserUpdated']);

        $data = $this->db->update(
            'outlet',
            [
                // 'outDcId' => $outDcId,
                // 'outCityId' => $outCityId,
                // 'outProvinceId' => $outProvinceId,
                // 'outIdCust' => $outIdCust,
                // 'outName' => $outName,
                // 'outAddress' => $outAddress,
                // 'outPhone' => $outPhone,
                // 'outContactPerson' => $outContactPerson,
                // 'outEmail' => $outEmail,
                // 'outPhoto' => $outLatitude,
                'outLatitude' => $outLongitude,
                'outLongitude' => $outLongitude,
                // 'outUserSaved' => $jwtid,
                // 'outUserUpdated' => $jwtid,
            ],
            [
                'outId' => security_single_post($post['outId']),
            ]
        );

        return $data;
    }
}
