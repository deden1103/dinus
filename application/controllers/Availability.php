<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Availability extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Availability_Model']);
    }

    public function index_get()
    {
        $page = 1;
        $searchtext = '';
        $type = '';

        if ($this->get('p')) {
            $page = intval($this->get('p'));
        }

        if ($this->get('s')) {
            $searchtext = security_single_post($this->get('s'));
        }

        $rows = 10;
        $offset = ($page - 1) * $rows;
        $list = $this->Availability_Model->list_product(
            $rows,
            $offset,
            $searchtext
        );
        $response = [
            'success' => true,
            'page' => $page,
            'next_page' => $page + 1,
            'data' => $list,
        ];
        $this->response($response, 200);
    }

    public function sub_get()
    {
        $page = 1;
        $searchtext = '';
        $type = '';

        $productId = intval($this->get('pid'));
        if ($this->get('p')) {
            $page = intval($this->get('p'));
        }

        if ($this->get('s')) {
            $searchtext = security_single_post($this->get('s'));
        }

        $rows = 10;
        $offset = ($page - 1) * $rows;
        $list = $this->Availability_Model->list_product_sub(
            $productId,
            $rows,
            $offset,
            $searchtext
        );
        $response = [
            'success' => true,
            'page' => $page,
            'next_page' => $page + 1,
            'data' => $list,
        ];
        $this->response($response, 200);
    }

    public function mylist_get()
    {
        $page = 1;
        $searchtext = '';
        $type = '';

        if ($this->get('p')) {
            $page = intval($this->get('p'));
        }

        if ($this->get('s')) {
            $searchtext = security_single_post($this->get('s'));
        }

        $rows = 10;
        $offset = ($page - 1) * $rows;
        $list = $this->Availability_Model->list_availability(
            $this->jwtData->id,
            $rows,
            $offset,
            $searchtext
        );
        $response = [
            'success' => true,
            'page' => $page,
            'next_page' => $page + 1,
            'data' => $list,
        ];
        $this->response($response, 200);
    }

    public function add_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $arrayValidation = [
            'avaUserId',
            'avaOutletId',
            'avaActId',
            'avaItemId',
            'avaDate',
            'avaGoodStock',
            'avaBadStock',
            'avaStatus',
            'avaUpdated',
            'avaUserSaved',
            'avaUserUpdated',
            'avaVisitId'
        ];

        foreach ($arrayValidation as $r) {
            $this->form_validation->set_rules($r, $r, 'trim|required');
        }
        $this->form_validation->set_message('required', '{field} harus diisi');

        if ($this->form_validation->run() == false) {
            $response = [
                'success' => false,
                'errors' => $this->form_validation->error_array(),
                'data' => [],
            ];
            $this->response($response, 200);
            exit();
        }

        $query = $this->Availability_Model->add_availability(
            $post,
            $this->jwtData->id
        );
        if ($query) {
            $response = [
                'success' => true,
                'errors' => '',
                'data' => $query,
            ];
        } else {
            $response = [
                'success' => false,
                'errors' => '',
                'data' => $query,
            ];
        }

        $this->response($response, 200);
    }
}
