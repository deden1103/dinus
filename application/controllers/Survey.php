<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Survey extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Survey_Model']);
    }

    public function index_get()
    {
        $page = 1;
        $searchtext = '';

        if ($this->get('p')) {
            $page = intval($this->get('p'));
        }

        if ($this->get('s')) {
            $searchtext = security_single_post($this->get('s'));
        }

        $rows = 10;
        $offset = ($page - 1) * $rows;
        $listNews = $this->Survey_Model->list_survey(
            $rows,
            $offset,
            $searchtext
        );
        $response = [
            'success' => true,
            'page' => $page,
            'next_page' => $page + 1,
            'data' => $listNews,
        ];
        $this->response($response, 200);
    }

    public function index_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

       
        $this->form_validation->set_rules('srvVisitId', 'visit id', 'trim|required');
        $this->form_validation->set_message('required', '{field} harus diisi');

        if ($this->form_validation->run() == false) {
            $response = [
                'success' => false,
                'errors' => $this->form_validation->error_array(),
                'data' => [],
            ];
            $this->response($response, 200);
            exit();
        }

        $query = $this->Survey_Model->add_survey($post);
        if ($query) {
            $response = [
                'success' => true,
                'data' => $query,
            ];
        } else {
            $response = [
                'success' => false,
                'data' => $query,
            ];
        }
        $this->response($response, 200);
    }

    public function edit_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());
        $query = $this->Survey_Model->edit_survey($post);
        if ($query) {
            $response = [
                'success' => true,
                'data' => [],
            ];
        } else {
            $response = [
                'success' => false,
                'data' => [],
                'id' => @$post['id']
            ];
        }

        $this->response($response, 200);
    }
}
