<?php
class User_Model extends CI_Model
{
    function detail($id)
    {
        $query = $this->db
            ->select('a.*, b.areaName as nama_area, c.mluDesc as level_user')
            ->from('user a')
            ->join('area b', 'b.areaId=a.userAreaId', 'LEFT')
            ->join('master_level_user c', 'c.mluId=a.userLevelId', 'LEFT')
            // ->order_by('a.itenSaved', 'DESC')
            ->where('a.userId', $id)
            ->where('a.userStatus', 1)
            ->get()
            ->result_array();
        // print_r($this->db->last_query());

        foreach ($query as $key => $value) {
            $query[$key]['userPhoto'] = $this->additional->image_profile(
                $value['userId'],
                $value['userPhoto']
            );
        }
        return $query;
    }

    function change($data)
    {
        $id = $data['userId']; // please read the below note
        $this->userAreaId = $data['userAreaId']; // please read the below note
        $this->userLevelId = $data['userLevelId'];
        $this->userCode = $data['userCode'];
        $this->userUsername = $data['userUsername'];
        $this->userPassword = $data['userPassword'];
        $this->userAreaName = $data['userAreaName'];
        $this->userIdDevice = $data['userIdDevice'];
        $this->userPhone = $data['userPhone'];
        $this->userEmail = $data['userEmail'];
        $this->userAddress = $data['userAddress'];
        $this->userCity = $data['userCity'];
        $this->userLatitude = $data['userLatitude'];
        $this->userLongitude = $data['userLongitude'];
        $this->userPhoto = $data['userPhoto'];
        $this->userUpdated = $data['userUpdated'];
        $this->db->where('userId', $id);
        $this->db->update('user', $this);
    }
}
