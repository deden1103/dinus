<?php
class News_Model extends CI_Model
{
    function list_news($limit='', $offset = '', $searchtext = '')
    {
       
        $sql = 'SELECT * FROM news';
        if ($searchtext) {
            $sql .= " WHERE newsTitle LIKE '%{$searchtext}%' ";
        }
        $sql .= ' ORDER BY newsUrgency DESC';
        if ($offset) {
            $sql .= " LIMIT {$limit} OFFSET {$offset}";
        }
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }
    }
}
