<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Visibility extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Visibility_Model']);
    }

    public function index_post()
    {
        $data = $this->post();
        $save = $this->Visibility_Model->add($data);
        // print_r($save);die;
        $files = $_FILES['photo'];
        $splitId = str_split($save['id']);
        foreach ($splitId as $k => $val) {
            $valArr[] = $val . "/";
        }
        $structure = implode($valArr);
        foreach($files['name'] as $key => $image){
            
            // if (!file_exists("../dinuss/assets/uploads/visibility/".$structure)) {
                //     mkdir("../dinuss/assets/uploads/visibility/" . $structure, 0777, true);
                // }
            if (!file_exists("../assets/uploads/".$structure)) {
                mkdir("../assets/uploads/" . $structure, 0777, true);
                // print_r('buat');die;
            }
            // $config['upload_path']          = '../dinuss/assets/uploads/visibility/' . $structure;
            $config['upload_path']       = '../assets/uploads/' . $structure;
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 20000000;
            
            $_FILES['images']['name'] = $files['name'][$key];
            $_FILES['images']['type'] = $files['type'][$key];
            $_FILES['images']['tmp_name'] = $files['tmp_name'][$key];
            $_FILES['images']['error'] = $files['error'][$key];
            $_FILES['images']['size'] = $files['size'][$key];
            // print_r('pisah');
            // print_r($_FILES);
            $this->load->library('upload', $config);
            if ( !$this->upload->do_upload('images')){
                $response = [
                    'data' => '',
                    'message' => 'Foto Gagal di Tambah',
                    'success' => false,
                ];
                $this->response($response, 400);

            } else {
                // print_r($this->upload->data());
                $data_upload = $this->upload->data();
                $data_upload['id'] = $save['id'];
                $datas = $this->Visibility_Model->add_photo($data_upload);
                $response = [
                    'data' => $datas,
                    'message' => 'Foto Berhasil di Tambah',
                    'success' => true,
                ];
            }
        }
        // print_r($response);die;
        $this->response($response, 200);
    }
}
}
