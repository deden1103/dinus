<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['User_Model']);
    }

    public function index_get()
    {
        $id = $this->jwtData->id;
        // print_r($id);die;

        $userData = $this->User_Model->detail($id);
        // if ($itenData) {
        $response = [
            // 'message' => 'Login Berhasil',
            'success' => true,
            'data' => $userData,
        ];

        $this->response($response, 200);
    }
    public function index_post()
    {
        $post = $this->post();

        // if(!empty($post['id'])){
            $data['userId'] = $this->jwtData->id;
            $data['userAreaId'] = $post['id_area'];
            $data['userLevelId'] = $post['id_level'];
            $data['userCode'] = $post['user_code'];
            $data['userUsername'] = $post['username'];
            $data['userPassword'] = base64_encode(hash('sha256', sha1($post['password'])));
            $data['userAreaName'] = $post['area'];
            $data['userIdDevice'] = $post['id_device'];
            $data['userPhone'] = $post['phone'];
            $data['userEmail'] = $post['email'];
            $data['userAddress'] = $post['address'];
            $data['userCity'] = $post['city'];
            $data['userLatitude'] = $post['latitude'];
            $data['userLongitude'] = $post['longitude'];
            $data['userPhoto'] = $post['photo'];
            $data['userUpdated'] = date("Y-m-d H:i:s");
        // }

        $userData = $this->User_Model->change($data);

        $response = [
            'data' => $userData,
            'message' => 'Data Berhasil di Ubah',
            'success' => true,
        ];

        $this->response($response, 200);
    }
}