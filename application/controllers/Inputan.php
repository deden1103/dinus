<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Inputan extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
       

    }

    public function index_get()
    {
       
    }

    public function inputan_get(){
        $sql = "SELECT * FROM tbl_kodepos WHERE inputed = 0 LIMIT 10000"; 
        $query = $this->db->query($sql)->result_array();
        
       
        foreach ($query as $k => $v) {
            $provid = $this->db->query("SELECT DISTINCT provId FROM provinces WHERE provName = '{$v['provinsi']}'")->row();
            $city = $this->db->query("SELECT DISTINCT cityId FROM city WHERE cityName = '{$v['kabupaten']}'")->row();
            $districtid = $this->db->query("SELECT DISTINCT districtId FROM district WHERE districtName = {$this->db->escape($v['kecamatan'])} ")->row();
            $query[$k]['batas'] = '----------------------------------------';
            $query[$k]['provid'] = $provid->provId;
            $query[$k]['cityid'] = $city->cityId;
            $query[$k]['districtid'] = $districtid->districtId;
            // print_r($query[$k]);
            $this->db->insert('district_sub',
            [
                'dsName' => $v['kelurahan'],
                'dsDistrictId' => $districtid->districtId,
                'dsProvId' =>  $provid->provId ,
                'dsCityId' => $city->cityId,
                'kodepost' =>$v['kodepos']
            ]);
            $this->db->update('tbl_kodepos',['inputed' => 1],['id' => $v['id']]);

        }
    }




    //UPDATE `tbl_kodepos` SET `inputed` = '0' WHERE `tbl_kodepos`.`id` >= 1;
   

}




