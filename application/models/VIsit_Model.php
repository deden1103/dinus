<?php
class Visit_Model extends CI_Model
{
    function detail($id)
    {
        $query = $this->db
            ->select('a.*, b.dcName as nama_distribusi, c.cityName as city, d.provName as province')
            ->from('outlet a')
            ->join('distribution_center b', 'b.dcId=a.outDcId', 'LEFT' )
            ->join('city c', 'c.cityId=a.outCityId', 'LEFT')
            ->join('provinces d', 'd.provId=a.outProvinceId', 'LEFT')
            // ->order_by('a.itenSaved', 'DESC')
            ->where("a.outId", $id)
            ->where("a.outStatus", 1)
            ->get();
            // print_r($this->db->last_query());
        return $query->row();
    }
    
    function add($data)
    {
        // $this->outDcId = $data['outDcId']; // please read the below note
        // $this->outCityId = $data['outCityId'];
        // $this->outProvinceId = $data['outProvinceId'];
        // $this->outIdCust = $data['outIdCust'];
        // $this->outName = $data['outName'];
        // $this->outAddress = $data['outAddress'];
        // $this->outPhone = $data['outPhone'];
        // $this->outContactPerson = $data['outContactPerson'];
        // $this->outEmail = $data['outEmail'];
        // $this->outPhoto = $data['outPhoto'];
        // $this->outLatitude = $data['outLatitude'];
        // $this->outLongitude = $data['outLongitude'];
        // $this->outSaved = $data['outSaved'];
        // $this->outUserSaved = $data['outUserSaved'];
        // $this->db->insert('outlet',$this);
        $this->db->insert('visit',$data);
    }
    
    function update($data, $id)
    {
        // $id = $data['outId']; // please read the below note
        // $this->outDcId = $data['outDcId']; // please read the below note
        // $this->outCityId = $data['outCityId'];
        // $this->outProvinceId = $data['outProvinceId'];
        // $this->outIdCust = $data['outIdCust'];
        // $this->outName = $data['outName'];
        // $this->outAddress = $data['outAddress'];
        // $this->outPhone = $data['outPhone'];
        // $this->outContactPerson = $data['outContactPerson'];
        // $this->outEmail = $data['outEmail'];
        // $this->outPhoto = $data['outPhoto'];
        // $this->outLatitude = $data['outLatitude'];
        // $this->outLongitude = $data['outLongitude'];
        // $this->outUpdated = $data['outUpdated'];
        // $this->outUserUpdated = $data['outUserUpdated'];
        $this->db->where('vstId', $id);
        $this->db->update('visit',$data);
    }
}