<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Uploads {
	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->library('image_lib');
	}

	public function upload_image($name="",$path="",$file_name="",$max_size="")
	{
		ini_set('post_max_size', '64M');
		ini_set('upload_max_filesize', '64M');

		empty($path) ? $path = "assets/uploads" : $path = $path;
		$max_size = empty($max_size) ? 1024*4 : $max_size;

		if (!is_dir($path)) {
			mkdir($path, 0777, TRUE);
		}

		$config['upload_path'] = $path;
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = $max_size;
		if($file_name!=""){
			$config['file_name'] = $file_name;
		} else {
			$config['encrypt_name'] = TRUE;
		}

		$this->ci->load->library('upload', $config);

		if (!$this->ci->upload->do_upload($name))
		{
			$data = array('error' => $this->ci->upload->display_errors());
		}
		else
		{
			$data = $this->ci->upload->data();
		}

		return $data;
	}

	function resize_image($source="",$path="",$width="",$height="", $ratio = TRUE) {

		empty($path) ? $path = "assets/uploads" : $path = $path;
		empty($height) ? $height = "125" : $height = $height;
		empty($width) ? $width = "140" : $width = $width;

		if (!is_dir($path)) {
			mkdir($path, 0777, TRUE);
		}

		$config['source_image'] = $source;
		$config['new_image'] = $path;
		$config['maintain_ratio'] = $ratio;
		$config['width'] = $width;
		$config['height'] = $height;

		$this->ci->image_lib->initialize($config);
		$res = $this->ci->image_lib->resize();
		$this->ci->image_lib->clear();

		if (!$res) {
			$data =  'thumb' . $this->ci->image_lib->display_errors();
		}else{
			$data = $res;
		}

		return $data;

	}


	public function upload_file($name="",$path="",$file_name="")
	{
		empty($path) ? $path = "assets/uploads" : $path = $path;

		if (!is_dir($path)) {
			mkdir($path, 0777, TRUE);
		}

		$config['upload_path'] = $path;
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc|xml|docx|GIF|JPG|PNG|JPEG|PDF|DOC|XML|DOCX|xls|xlsx';
		$config['max_size'] = '2000';
		if($file_name!=""){
			$config['file_name'] = $file_name;
		} else {
			$config['encrypt_name'] = TRUE;
		}

		$this->ci->load->library('upload', $config);

		if (!$this->ci->upload->do_upload($name))
		{
			$data = array('error' => $this->ci->upload->display_errors());
		}
		else
		{
			$data = $this->ci->upload->data();
		}

		return $data;
	}


	public function resize_img($path,$file_ext,$idArim)
    {
        $size_w=array(224,263,300,512,683,840);
        $size_h=array(153,180,206,351,468,576);

        for($c=0;$c<6;$c++)
        { 
            $config['image_library']    = 'gd2';
            $config['source_image']     = $path;
            $config['create_thumb']     = true;
            $config['maintain_ratio']   = true;
            $config['width']            = $size_w[$c];
            $config['height']           = $size_h[$c];  
            $config['create_thumb']     = 'false';
            $config['thumb_marker']     = ''; 
            $config['new_image']        = $idArim.'_'.$size_w[$c].'x'.$size_h[$c].$file_ext;
             
         
            $this->ci->image_lib->initialize($config);
            $this->ci->image_lib->resize();
            $this->ci->image_lib->clear();
        }
    }

	function video_convert($path='',$path1=''){
        $q='ffmpeg -ss 00:00:05  -i '.$path.'  -t 00:00:10 -strict -2  '.$path1.'';
        echo exec($q);

    }

}
