<?php
class Master_Model extends CI_Model
{
    function provinces()
    {
        $sql = 'SELECT * FROM provinces';
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }
    }

    function city($provid)
    {
        $sql = "SELECT * FROM city WHERE cityProvId = {$provid}";
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }
    }


    function kecamatan($cityid)
    {
        $sql = "SELECT * FROM district WHERE districtCityId = {$cityid}";
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }
    }

    function kelurahan($kecid)
    {
        $sql = "SELECT * FROM district_sub WHERE dsDistrictId = {$kecid}";
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }
    }

}
