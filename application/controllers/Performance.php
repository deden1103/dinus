<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Performance extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Performance_Model']);
    }

    public function index_get()
    {
        // $itenData = $this->Performance_Model->detail($this->jwtData->id);
        $bulan = date('m');
        $tahun = date('Y');

        if ($this->get('bulan')) {
            $bulan = $this->get('bulan');
        }

        if ($this->get('tahun')) {
            $tahun = $this->get('tahun');
        }

        //===================
        $count_sudah = $this->Performance_Model->count_sudah(
            $this->jwtData->id,
            $bulan,
            $tahun
        );
        $target_visit = $this->Performance_Model->target_visit(
            $this->jwtData->id,
            $bulan,
            $tahun
        );
        $count_belum = $target_visit - $count_sudah;
        if ($target_visit == 0) {
            $response = [
                'message' => 'success',
                'success' => true,
                'bulan' => $bulan,
                'tahun' => $tahun,
                'status_outlet' => [],
                'hasil_photo' => [],
                'insentif' => [],
            ];
            $this->response($response, 200);
            exit();
        }
        $persen_sudah = round(($count_sudah / $target_visit) * 100);
        $persen_belum = round(($count_belum / $target_visit) * 100);
        $persen_tidak = round(($count_belum / $target_visit) * 100);
        $persen_target = 100;
        $status_outlet = [
            [
                'title' => 'Belum Dikunjungi',
                'qty' => $count_belum,
                'persentase' => $persen_belum . '%',
            ],
            [
                'title' => 'Sudah Dikunjungi',
                'qty' => $count_sudah,
                'persentase' => $persen_sudah . '%',
            ],
            [
                'title' => 'Tidak Dikunjungi',
                'qty' => $count_belum,
                'persentase' => $persen_belum . '%',
            ],
            [
                'title' => 'Target',
                'qty' => $target_visit,
                'persentase' => $persen_target . '%',
            ],
        ];
        //=============
        $photo_ok = $this->Performance_Model->count_photo(
            $this->jwtData->id,
            'ok',
            $bulan,
            $tahun
        );
        $photo_not_ok = $this->Performance_Model->count_photo(
            $this->jwtData->id,
            'notok',
            $bulan,
            $tahun
        );
        $photo_all = $this->Performance_Model->count_photo(
            $this->jwtData->id,
            'all',
            $bulan,
            $tahun
        );
        $persen_photo_ok = round(($photo_ok / $photo_all) * 100);
        $persen_photo_not_ok = round(($photo_not_ok / $photo_all) * 100);
        $persen_photo_all = 100;
        $hasil_photo = [
            [
                'title' => 'Photo OK',
                'qty' => $photo_ok,
                'persentase' => $persen_photo_ok . '%',
            ],
            [
                'title' => 'Photo Tidak OK',
                'qty' => $photo_not_ok,
                'persentase' => $persen_photo_not_ok . '%',
            ],
            [
                'title' => 'Total Photo',
                'qty' => $photo_all,
                'persentase' => $persen_photo_all . '%',
            ],
        ];

        $insentif_one = 0;
        $insentif_two = 0;
        if ($persen_sudah >= 80 && $persen_sudah <= 95) {
            $insentif_one = '1.700.000';
        }

        if ($persen_sudah > 95) {
            $insentif_two = '1.900.000';
        }

        $insentif = [
            [
                'title' => 'Pencapaian 80 ‐ 95 %',
                'qty' => $insentif_one,
            ],
            [
                'title' => 'Pencaoaian > 95 %',
                'qty' => $insentif_two,
            ],
        ];

        $response = [
            'message' => 'success',
            'success' => true,
            'bulan' => $bulan,
            'tahun' => $tahun,
            'status_outlet' => $status_outlet,
            'hasil_photo' => $hasil_photo,
            'insentif' => $insentif,
        ];

        $this->response($response, 200);
    }
}
