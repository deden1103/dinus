<?php
class Survey_Model extends CI_Model
{
    function list_survey($limit = '', $offset = '', $searchtext = '')
    {
        $sql = 'SELECT * FROM survey';
        $sql .=
            ' LEFT JOIN master_activity ON survey.srvActId = master_activity.actId';
        if ($searchtext) {
            $sql .= " WHERE srvSurvey LIKE '%{$searchtext}%' ";
        }
        // $sql .= ' ORDER BY newsUrgency DESC';
        if ($offset) {
            $sql .= " LIMIT {$limit} OFFSET {$offset}";
        }
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }
    }

    function add_survey($post)
    {
        $insert = $this->db->insert('survey', [
            'srvUserId' => security_single_post($post['srvUserId']),
            'srvOutletId' => security_single_post($post['srvOutletId']),
            'srvActId' => security_single_post($post['srvActId']),
            'srvDate' => security_single_post($post['srvDate']),
            'srvSurvey' => security_single_post($post['srvSurvey']),
            'srvSaved' => security_single_post($post['srvSaved']),
            'srvUpdated' => security_single_post($post['srvUpdated']),
            'srvVisitId' => security_single_post($post['srvVisitId']),
        ]);

        $insertid = $this->db->insert_id();
        $data = [];
        if ($insert) {
            return true;
        } else {
            return false;
        }
    }

    function edit_survey($post)
    {
        $insert = $this->db->update(
            'survey',
            [
                'srvUserId' => security_single_post($post['srvUserId']),
                'srvOutletId' => security_single_post($post['srvOutletId']),
                'srvActId' => security_single_post($post['srvActId']),
                'srvDate' => security_single_post($post['srvDate']),
                'srvSurvey' => security_single_post($post['srvSurvey']),
                'srvSaved' => security_single_post($post['srvSaved']),
                'srvUpdated' => security_single_post($post['srvUpdated']),
            ],
            ['srvId' => security_single_post($post['id'])]
        );

        if ($insert) {
            return true;
        } else {
            return false;
        }
    }
}
