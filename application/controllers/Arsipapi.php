<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Arsipapi extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_Image');
        $this->load->model('M_global');
    }
    public function index_get()
    {
        $response = array(
            'message' => 'Welcome to Asip API api',
        );
        $this->response($response, 200);
    }

    public function add_submit_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_message('required', '{field} harus diisi');

        $user = @$post['useradmin'];
        
        $path='';
        if (@$post['tag']) {
            $tag = implode(',', @$post['tag']);
        }

        $dataArsip = array(
            'arsiTitle' => @$post['title'],
            'arsiDesc' => @$post['desc'],
            'arsiCat' => @$post['cat'],
            // 'arsiTag' =>  ",".$tag.",",
            'arsiUserIdSaved' => $user,
            'publishDate' => @$post['publish_date'],
        );

       
        $this->M_global->insert('arsip', $dataArsip); 
        $idArsi = $this->db->insert_id();
        insertAudit($this->db->last_query(), $_SERVER['HTTP_USER_AGENT'], $_SERVER['REMOTE_ADDR'],@$post['useradmin']);

        if ($post['tag']) {
            foreach ($post['tag'] as $kk => $vv) {
                $this->db->insert('arsip_tag', [
                    'arsipId' => $idArsi,
                    'arsipMasterTagId' => $vv,
                ]);
            }
        }


        if (!empty($_FILES['gambar']['name'])) {
            $filesCount = count($_FILES['gambar']['name']);

            for ($i = 0; $i < $filesCount; $i++) {
                $data = array(
                    'arimArsiId' => $idArsi,
                    'arimUserIdSaved' => $user,
                );
                $this->M_global->insert('arsip_images', $data);
                $idArim = $this->db->insert_id();
                //upload image
                $path = '/home/html/lms/cms/uploads/arsip';
                $fId = str_split($idArim);
                foreach ($fId as $v) {
                    $path .= "/{$v}";
                    if (!is_dir($path)) {
                        mkdir($path, 0777);
                    }
                }
                $_FILES['ImageFiles']['name'] = $_FILES['gambar']['name'][$i];
                $_FILES['ImageFiles']['type'] = $_FILES['gambar']['type'][$i];
                $_FILES['ImageFiles']['tmp_name'] = $_FILES['gambar']['tmp_name'][$i];
                $_FILES['ImageFiles']['error'] = $_FILES['gambar']['error'][$i];
                $_FILES['ImageFiles']['size'] = $_FILES['gambar']['size'][$i];
                // File upload configuration
                $config['upload_path'] = $path;
                $config['allowed_types'] = '*';
                $config['max_size'] = '0';
                $config['file_name'] = $idArim;
                $this->load->library('upload');
                $this->upload->initialize($config);
                $this->upload->do_upload('ImageFiles');
                $data_file = $this->upload->data();
                $file_ext = $data_file['file_ext'];
                $type = substr($file_ext, 1);
                $img = $path . '/' . $idArim . $file_ext;
                // //resize image
                $this->load->library('uploads');
                $this->uploads->resize_img($img, $file_ext, $idArim);
                $data_image = array(
                    'arimFileType' => $type,
                );
                $where = array(
                    'arimId' => $idArim,
                );
                $this->M_global->update('arsip_images', $data_image, $where);
            }
        }

        if ($this->form_validation->run() == true) {

            $response = array(
                // 'data' => $post,
                // 'path' => $path,
                // 'img' => $img,
                'message' => 'Tambah Data berhasil Berhasil',
                'success' => true,
            );
        } else {
            $response = array(
                'message' => 'Maaf, Terjadi Kesalahan',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            );
        }
        $this->response($response, 200);
    }



    public function edit_submit_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_message('required', '{field} harus diisi');

        $user = @$post['useradmin'];
        $idArsi = @$post['idArsi'];
        
        $path='';
        if (@$post['tag']) {
            $tag = implode(',', @$post['tag']);
        }

        $dataArsip = array(
            'arsiTitle' => @$post['title'],
            'arsiDesc' => @$post['desc'],
            'arsiCat' => @$post['cat'],
            // 'arsiTag' =>  ",".$tag.",",
            'arsiUserIdSaved' => $user,
            'publishDate' => @$post['publish_date'],
        );

        $where = array(
            'arsiId' => $idArsi,
        );
       
        $save = $this->M_global->update('arsip', $dataArsip, $where);
        insertAudit($this->db->last_query(), $_SERVER['HTTP_USER_AGENT'], $_SERVER['REMOTE_ADDR'],@$post['useradmin']);
        if ($post['tag']) {
            $this->db->delete('arsip_tag', ['arsipId' => $idArsi]);
            foreach ($post['tag'] as $kk => $vv) {
                $this->db->insert('arsip_tag', [
                    'arsipId' => $idArsi,
                    'arsipMasterTagId' => $vv,
                ]);
            }
        }


        if (!empty($_FILES['gambar']['name'])) {
            $filesCount = count($_FILES['gambar']['name']);

            for ($i = 0; $i < $filesCount; $i++) {
                $data = array(
                    'arimArsiId' => $idArsi,
                    'arimUserIdSaved' => $user,
                );
                $this->M_global->insert('arsip_images', $data);
                $idArim = $this->db->insert_id();
                //upload image
                $path = '/home/html/lms/cms/uploads/arsip';
                $fId = str_split($idArim);
                foreach ($fId as $v) {
                    $path .= "/{$v}";
                    if (!is_dir($path)) {
                        mkdir($path, 0777);
                    }
                }
                $_FILES['ImageFiles']['name'] = $_FILES['gambar']['name'][$i];
                $_FILES['ImageFiles']['type'] = $_FILES['gambar']['type'][$i];
                $_FILES['ImageFiles']['tmp_name'] = $_FILES['gambar']['tmp_name'][$i];
                $_FILES['ImageFiles']['error'] = $_FILES['gambar']['error'][$i];
                $_FILES['ImageFiles']['size'] = $_FILES['gambar']['size'][$i];
                // File upload configuration
                $config['upload_path'] = $path;
                $config['allowed_types'] = '*';
                $config['max_size'] = '0';
                $config['file_name'] = $idArim;
                $this->load->library('upload');
                $this->upload->initialize($config);
                $this->upload->do_upload('ImageFiles');
                $data_file = $this->upload->data();
                $file_ext = $data_file['file_ext'];
                $type = substr($file_ext, 1);
                $img = $path . '/' . $idArim . $file_ext;
                // //resize image
                $this->load->library('uploads');
                $this->uploads->resize_img($img, $file_ext, $idArim);
                $data_image = array(
                    'arimFileType' => $type,
                );
                $where = array(
                    'arimId' => $idArim,
                );
                $this->M_global->update('arsip_images', $data_image, $where);
            }
        }

        if ($this->form_validation->run() == true) {

            $response = array(
                'data' => $post,
                // 'path' => $path,
                // 'img' => $img,
                'message' => 'Tambah Data berhasil Berhasil',
                'success' => true,
            );
        } else {
            $response = array(
                'message' => 'Maaf, Terjadi Kesalahan',
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            );
        }
        $this->response($response, 200);
    }

}
