<?php
class Activity_Model extends CI_Model
{
    function list_activity(
        $type = '',
        $limit = '',
        $offset = '',
        $searchtext = ''
    ) {
        $sql = 'SELECT * FROM master_activity';
        if ($searchtext) {
            $sql .= " WHERE actName LIKE '%{$searchtext}%' ";
        }

        if ($type) {
            $sql .= " WHERE actType = {$type} ";
        }

        // $sql .= ' ORDER BY newsUrgency DESC';
        // if ($offset) {
        //     $sql .= " LIMIT {$limit} OFFSET {$offset}";
        // }
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }
    }
}
