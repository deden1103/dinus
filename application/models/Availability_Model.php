<?php
class Availability_Model extends CI_Model
{
    function list_product($limit = '', $offset = '', $searchtext = '')
    {
        $sql = 'SELECT * FROM master_product';
        if ($searchtext) {
            $sql .= " WHERE mpDesc LIKE '%{$searchtext}%' ";
        }

        // $sql .= ' ORDER BY newsUrgency DESC';
        if ($offset) {
            $sql .= " LIMIT {$limit} OFFSET {$offset}";
        }
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }
    }

    function list_availability(
        $jwtdataid,
        $limit = '',
        $offset = '',
        $searchtext = ''
    ) {
        $sql = 'SELECT * FROM availability';
        $sql .= ' LEFT JOIN item ON availability.avaItemId = item.itemId';
        $sql .= ' LEFT JOIN outlet ON availability.avaOutletId = outlet.outId';
        $sql .= " WHERE avaUserId = {$jwtdataid}";
        if ($searchtext) {
            $sql .= " AND itemName LIKE '%{$searchtext}%' ";
        }
        // $sql .= ' ORDER BY newsUrgency DESC';
        // if ($offset) {
        //     $sql .= " LIMIT {$limit} OFFSET {$offset}";
        // }
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }
    }

    function list_product_sub(
        $pid = '',
        $limit = '',
        $offset = '',
        $searchtext = ''
    ) {
        $sql = 'SELECT * FROM sub_product';
        $sql .= " WHERE spProdId = {$pid} ";
        if ($searchtext) {
            $sql .= " AND spName LIKE '%{$searchtext}%' ";
        }

        // $sql .= ' ORDER BY newsUrgency DESC';
        if ($offset) {
            $sql .= " LIMIT {$limit} OFFSET {$offset}";
        }
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }
    }

    function add_availability($post, $jwtdataid)
    {
        try {
            $avaUserId = security_single_post(@$post['avaUserId']);
            $avaOutletId = security_single_post(@$post['avaOutletId']);
            $avaActId = security_single_post(@$post['avaActId']);
            $avaItemId = security_single_post(@$post['avaItemId']);
            $avaDate = security_single_post(@$post['avaDate']);
            $avaGoodStock = security_single_post(@$post['avaGoodStock']);
            $avaBadStock = security_single_post(@$post['avaBadStock']);
            $avaStatus = security_single_post(@$post['avaStatus']);
            $avaUserSaved = security_single_post(@$post['avaUserSaved']);
            $avaUserUpdated = security_single_post(@$post['avaUserUpdated']);
            $avaUpdated = security_single_post(@$post['avaUpdated']);
            $avaVisitId = security_single_post(@$post['avaVisitId']);
            $insert = $this->db->insert('availability', [
                'avaUserId' => $jwtdataid,
                'avaVisitId' => $avaVisitId,
                'avaOutletId' => $avaOutletId,
                'avaActId' => $avaActId,
                'avaItemId' => $avaItemId,
                'avaDate' => $avaDate,
                'avaGoodStock' => $avaGoodStock,
                'avaBadStock' => $avaBadStock,
                'avaStatus' => $avaStatus,
                'avaSaved' => date('Y-m-d'),
                'avaUpdated' => $avaUpdated,
                'avaUserSaved' => $jwtdataid,
                'avaUserUpdated' => $jwtdataid,
            ]);
    
           
    
            if ($insert) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $er) {
            return false;
        }
        
    }
}
