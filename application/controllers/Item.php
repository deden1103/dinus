<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Item extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Item_Model']);
    }

    public function index_get()
    {
        $page = 1;
        $searchtext = '';
        $type = '';

        if ($this->get('p')) {
            $page = intval($this->get('p'));
        }

        if ($this->get('s')) {
            $searchtext = security_single_post($this->get('s'));
        }

        $rows = 10;
        $offset = ($page - 1) * $rows;
        $list = $this->Item_Model->list_item($rows, $offset, $searchtext);
        $response = [
            'success' => true,
            'page' => $page,
            'next_page' => $page + 1,
            'data' => $list,
        ];
        $this->response($response, 200);
    }

    public function list_get()
    {
        $page = 1;
        $searchtext = '';
        $type = '';
        $sub_id = '';

        if ($this->get('p')) {
            $page = intval($this->get('p'));
        }

        if ($this->get('s')) {
            $searchtext = security_single_post($this->get('s'));
        }

        if ($this->get('sub')) {
            $sub_id = intval($this->get('sub'));
        } else {
            $response = [
                'success' => false,
                'message' => 'sub id wajib diisi',
                'page' => $page,
                'next_page' => $page + 1,
                'data' => [],
            ];
            $this->response($response, 200);
            exit();
        }

        $rows = 10;
        $offset = ($page - 1) * $rows;
        $list = $this->Item_Model->list_item_by_sub(
            $rows,
            $offset,
            $searchtext,
            $sub_id
        );
        $response = [
            'success' => true,
            'message' => '',
            'page' => $page,
            'next_page' => $page + 1,
            'data' => $list,
        ];
        $this->response($response, 200);
    }

    public function dcitem_get()
    {
        $dcid=security_single_post($this->get('dcid')); 

        $query = $this->Item_Model->dcitem($dcid);
        $response = [
            'success' => true,
            'data' => $query,
        ];
        $this->response($response, 200);
    }

}
