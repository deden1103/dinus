<?php

if ( ! function_exists('showDate'))
{
	function showDate($date="",$day="")
	{						
		$array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
		$array_bulan = array("1" => "Januari", "2" => "Februari", "3" => "Maret", "4" => "April", "5" => "Mei", "6" => "Juni", "7" => "Juli",
			"8" => "Agustus", "9" => "September", "01" => "Januari", "02" => "Februari", "03" => "Maret", "04" => "April", "05" => "Mei", "06" => "Juni", "07" => "Juli",
			"08" => "Agustus", "09" => "September", "10" => "Oktober", "11" => "November", "12" => "December");

		if ($day){
			$text = date('d', $date)." ".$array_bulan[date('m', $date)]." ".date('Y', $date);
		}else{
			$text = $array_hari[date('N', $date)].", ".date('d', $date)." ".$array_bulan[date('m', $date)]." ".date('Y', $date);
		}

		return $text;
	}
}

if(!function_exists('qrcode'))
{
	function qrcode()
	{		
		$alphabet = "ABCDEFGHIJKLMNOPQRSTUWXYZ0123456789".date('ymdhis');
		$pass = array();
		$alphaLength = strlen($alphabet) - 1; 
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass);
	}
}

if(!function_exists('getUser'))
{
	function getUser($field)
	{
		$CI =& get_instance();
		$CI->load->model('account/Model_account');
		$query = $CI->Model_account->detail();
		return $query->$field;
	}

}

if(!function_exists('getCaptcha'))
{
	function getCaptcha($par)
	{
		$CI =& get_instance();
		$CI->load->library('recaptcha');
		if ($par == 'widget') {
			return $CI->recaptcha->getWidget();
		}else{
			return $CI->recaptcha->getScriptTag();
		}
	}

}



function xss($val = '') {
    $val = preg_replace('/([\x00-\x08][\x0b-\x0c][\x0e-\x20])/', '', $val);
    $search = 'abcdefghijklmnopqrstuvwxyz';
    $search .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $search .= '1234567890!@#$%^&*()';
    $search .= '~`";:?+={}[]-_|\'\\';
    for ($i = 0; $i < strlen($search); $i++) {
        $val = preg_replace('/(&#[x|X]0{0,8}' . dechex(ord($search[$i])) . ';?)/i', $search[$i], $val); // with a ;
        $val = preg_replace('/(&#0{0,8}' . ord($search[$i]) . ';?)/', $search[$i], $val); // with a ;
    }
    $ra1 = Array('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'style', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base');
    $ra2 = Array('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload');
    $ra = array_merge($ra1, $ra2);
    $found = true;
    while ($found == true) {
        $val_before = $val;
        for ($i = 0; $i < sizeof($ra); $i++) {
            $pattern = '/';
            for ($j = 0; $j < strlen($ra[$i]); $j++) {
                if ($j > 0) {
                    $pattern .= '(';
                    $pattern .= '(&#[x|X]0{0,8}([9][a][b]);?)?';
                    $pattern .= '|(&#0{0,8}([9][10][13]);?)?';
                    $pattern .= ')?';
                }
                $pattern .= $ra[$i][$j];
            }
            $pattern .= '/i';
            $replacement = substr($ra[$i], 0, 2) . '<x>' . substr($ra[$i], 2);
            $val = preg_replace($pattern, $replacement, $val);
            if ($val_before == $val) {
                $found = false;
            }
        }
    }
    return $val;
}

 function anti_injection($data)
{
    $filter = addslashes(strip_tags(htmlspecialchars($data, ENT_QUOTES)));
    $filter = str_replace('*', 's', $filter);
    $filter = str_replace('\\', 'b', $filter);
    $filter = str_replace('%', 'p', $filter);
    $filter = str_replace('<script>', '"', $filter);
    // $filter = str_replace('or', '"', $filter);
    $filter = str_replace('#', '"', $filter);
    $filter = str_replace('---', '"', $filter);
    $filter = str_replace('union', '"', $filter);
    $filter = str_replace('UNION', '"', $filter);
    $filter = str_replace('select', '"', $filter);
    $filter = str_replace('SELECT', '"', $filter);
    $filter = str_replace('hostname', '"', $filter);
    $filter = str_replace('HOSTNAME', '"', $filter);
    $filter = str_replace('USER()', '"', $filter);
    $filter = str_replace('user()', '"', $filter);
    $filter = str_replace('DATABASE', '"', $filter);
    $filter = str_replace('database', '"', $filter);
    $filter = str_replace('"', '', $filter);
    $filter = strip_tags($filter, '<img>');
    return $filter;
}

 function security_single_post($text = '') {
    return anti_injection(xss($text));
}