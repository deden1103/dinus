<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Itenary extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Itenary_Model']);
    }

    public function index_get()
    {
        $d=security_single_post($this->get('d'));
        $m=security_single_post($this->get('m'));
        $y=security_single_post($this->get('y'));
        $itenData = $this->Itenary_Model->detail($this->jwtData->id,$d,$m,$y);
        $response = [
            'message' => 'success',
            'success' => true,
            'data' => $itenData,
        ];

        $this->response($response, 200);
    }
}
