<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Additional
{

    public function image_outlet($id,$filetype)
    {
        $CI = &get_instance();
        $full = $id;
        $split_id = str_split($id);
        $path_folder_image = implode('/', $split_id);
        return URL_IMG_OUTLET . "{$path_folder_image}/{$full}.{$filetype}";
    }

    public function image_profile($id,$filetype)
    {
        $CI = &get_instance();
        $full = $id;
        $split_id = str_split($id);
        $path_folder_image = implode('/', $split_id);
        return URL_IMG_PROFILE . "{$path_folder_image}/{$full}.{$filetype}";
    }

    public function image_item($id,$filetype)
    {
        $CI = &get_instance();
        $full = $id;
        $split_id = str_split($id);
        $path_folder_image = implode('/', $split_id);
        return URL_IMG_ITEM . "{$path_folder_image}/{$full}.{$filetype}";
    }


    
    // public function restyle_text($input)
    // {
    //     $input = number_format($input);
    //     $input_count = substr_count($input, ",");
    //     if ($input_count != "0") {
    //         if ($input_count == "1") {
    //             return substr($input, 0, -4) . "k";
    //         } else if ($input_count == "2") {
    //             return substr($input, 0, -8) . "mil";
    //         } else if ($input_count == "3") {
    //             return substr($input, 0, -12) . "bil";
    //         } else {
    //             return;
    //         }
    //     } else {
    //         return $input;
    //     }
    // }

    // public function cut_text($text = '', $num_char = '')
    // {
    //     $cut_text = substr($text, 0, $num_char);
    //     if ($text{$num_char - 1} != ' ') { // jika huruf ke 50 (50 - 1 karena index dimulai dari 0) buka  spasi
    //         $new_pos = strrpos($cut_text, ' '); // cari posisi spasi, pencarian dari huruf terakhir
    //         $cut_text = substr($text, 0, $new_pos);
    //     }

    //     return $cut_text . '...';
    // }

    // public function slugify($text)
    // {
    //     $text = str_replace('\'', '', $text);
    //     // replace non letter or digits by -
    //     $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    //     // transliterate
    //     $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    //     // remove unwanted characters
    //     $text = preg_replace('~[^-\w]+~', '', $text);
    //     // trim
    //     $text = trim($text, '-');
    //     // remove duplicate -
    //     $text = preg_replace('~-+~', '-', $text);
    //     // lowercase
    //     $text = strtolower($text);
    //     if (empty($text)) {
    //         return 'n-a';
    //     }
    //     return $text;
    // }

    // public function indonesian_date_only($timestamp = '', $date_format = 'j F Y', $suffix = 'WIB')
    // {
    //     if (trim($timestamp) == '') {
    //         $timestamp = time();
    //     } elseif (!ctype_digit($timestamp)) {
    //         $timestamp = strtotime($timestamp);
    //     }
    //     # remove S (st,nd,rd,th) there are no such things in indonesia :p
    //     $date_format = preg_replace("/S/", "", $date_format);
    //     $pattern = array(
    //         '/Mon[^day]/',
    //         '/Tue[^sday]/',
    //         '/Wed[^nesday]/',
    //         '/Thu[^rsday]/',
    //         '/Fri[^day]/',
    //         '/Sat[^urday]/',
    //         '/Sun[^day]/',
    //         '/Monday/',
    //         '/Tuesday/',
    //         '/Wednesday/',
    //         '/Thursday/',
    //         '/Friday/',
    //         '/Saturday/',
    //         '/Sunday/',
    //         '/Jan[^uary]/',
    //         '/Feb[^ruary]/',
    //         '/Mar[^ch]/',
    //         '/Apr[^il]/',
    //         '/May/',
    //         '/Jun[^e]/',
    //         '/Jul[^y]/',
    //         '/Aug[^ust]/',
    //         '/Sep[^tember]/',
    //         '/Oct[^ober]/',
    //         '/Nov[^ember]/',
    //         '/Dec[^ember]/',
    //         '/January/',
    //         '/February/',
    //         '/March/',
    //         '/April/',
    //         '/June/',
    //         '/July/',
    //         '/August/',
    //         '/September/',
    //         '/October/',
    //         '/November/',
    //         '/December/',
    //     );
    //     $replace = array(
    //         'Sen',
    //         'Sel',
    //         'Rab',
    //         'Kam',
    //         'Jum',
    //         'Sab',
    //         'Min',
    //         'Senin',
    //         'Selasa',
    //         'Rabu',
    //         'Kamis',
    //         'Jumat',
    //         'Sabtu',
    //         'Minggu',
    //         'Jan',
    //         'Feb',
    //         'Mar',
    //         'Apr',
    //         'Mei',
    //         'Jun',
    //         'Jul',
    //         'Ags',
    //         'Sep',
    //         'Okt',
    //         'Nov',
    //         'Des',
    //         'Januari',
    //         'Februari',
    //         'Maret',
    //         'April',
    //         'Juni',
    //         'Juli',
    //         'Agustus',
    //         'September',
    //         'Oktober',
    //         'November',
    //         'Desember',
    //     );
    //     $date = date($date_format, $timestamp);
    //     $date = preg_replace($pattern, $replace, $date);
    //     $date = "{$date}";
    //     return $date;
    // }

    // public function indonesian_date($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = 'WIB')
    // {
    //     if (trim($timestamp) == '') {
    //         $timestamp = time();
    //     } elseif (!ctype_digit($timestamp)) {
    //         $timestamp = strtotime($timestamp);
    //     }
    //     # remove S (st,nd,rd,th) there are no such things in indonesia :p
    //     $date_format = preg_replace("/S/", "", $date_format);
    //     $pattern = array(
    //         '/Mon[^day]/',
    //         '/Tue[^sday]/',
    //         '/Wed[^nesday]/',
    //         '/Thu[^rsday]/',
    //         '/Fri[^day]/',
    //         '/Sat[^urday]/',
    //         '/Sun[^day]/',
    //         '/Monday/',
    //         '/Tuesday/',
    //         '/Wednesday/',
    //         '/Thursday/',
    //         '/Friday/',
    //         '/Saturday/',
    //         '/Sunday/',
    //         '/Jan[^uary]/',
    //         '/Feb[^ruary]/',
    //         '/Mar[^ch]/',
    //         '/Apr[^il]/',
    //         '/May/',
    //         '/Jun[^e]/',
    //         '/Jul[^y]/',
    //         '/Aug[^ust]/',
    //         '/Sep[^tember]/',
    //         '/Oct[^ober]/',
    //         '/Nov[^ember]/',
    //         '/Dec[^ember]/',
    //         '/January/',
    //         '/February/',
    //         '/March/',
    //         '/April/',
    //         '/June/',
    //         '/July/',
    //         '/August/',
    //         '/September/',
    //         '/October/',
    //         '/November/',
    //         '/December/',
    //     );
    //     $replace = array(
    //         'Sen',
    //         'Sel',
    //         'Rab',
    //         'Kam',
    //         'Jum',
    //         'Sab',
    //         'Min',
    //         'Senin',
    //         'Selasa',
    //         'Rabu',
    //         'Kamis',
    //         'Jumat',
    //         'Sabtu',
    //         'Minggu',
    //         'Jan',
    //         'Feb',
    //         'Mar',
    //         'Apr',
    //         'Mei',
    //         'Jun',
    //         'Jul',
    //         'Ags',
    //         'Sep',
    //         'Okt',
    //         'Nov',
    //         'Des',
    //         'Januari',
    //         'Februari',
    //         'Maret',
    //         'April',
    //         'Juni',
    //         'Juli',
    //         'Agustus',
    //         'September',
    //         'Oktober',
    //         'November',
    //         'Desember',
    //     );
    //     $date = date($date_format, $timestamp);
    //     $date = preg_replace($pattern, $replace, $date);
    //     $date = "{$date} {$suffix}";
    //     return $date;
    // }
    // public function indonesian_date_no_time($timestamp = '', $date_format = 'l, j F Y')
    // {
    //     if (trim($timestamp) == '') {
    //         $timestamp = time();
    //     } elseif (!ctype_digit($timestamp)) {
    //         $timestamp = strtotime($timestamp);
    //     }
    //     # remove S (st,nd,rd,th) there are no such things in indonesia :p
    //     $date_format = preg_replace("/S/", "", $date_format);
    //     $pattern = array(
    //         '/Mon[^day]/',
    //         '/Tue[^sday]/',
    //         '/Wed[^nesday]/',
    //         '/Thu[^rsday]/',
    //         '/Fri[^day]/',
    //         '/Sat[^urday]/',
    //         '/Sun[^day]/',
    //         '/Monday/',
    //         '/Tuesday/',
    //         '/Wednesday/',
    //         '/Thursday/',
    //         '/Friday/',
    //         '/Saturday/',
    //         '/Sunday/',
    //         '/Jan[^uary]/',
    //         '/Feb[^ruary]/',
    //         '/Mar[^ch]/',
    //         '/Apr[^il]/',
    //         '/May/',
    //         '/Jun[^e]/',
    //         '/Jul[^y]/',
    //         '/Aug[^ust]/',
    //         '/Sep[^tember]/',
    //         '/Oct[^ober]/',
    //         '/Nov[^ember]/',
    //         '/Dec[^ember]/',
    //         '/January/',
    //         '/February/',
    //         '/March/',
    //         '/April/',
    //         '/June/',
    //         '/July/',
    //         '/August/',
    //         '/September/',
    //         '/October/',
    //         '/November/',
    //         '/December/',
    //     );
    //     $replace = array(
    //         'Sen',
    //         'Sel',
    //         'Rab',
    //         'Kam',
    //         'Jum',
    //         'Sab',
    //         'Min',
    //         'Senin',
    //         'Selasa',
    //         'Rabu',
    //         'Kamis',
    //         'Jumat',
    //         'Sabtu',
    //         'Minggu',
    //         'Jan',
    //         'Feb',
    //         'Mar',
    //         'Apr',
    //         'Mei',
    //         'Jun',
    //         'Jul',
    //         'Ags',
    //         'Sep',
    //         'Okt',
    //         'Nov',
    //         'Des',
    //         'Januari',
    //         'Februari',
    //         'Maret',
    //         'April',
    //         'Juni',
    //         'Juli',
    //         'Agustus',
    //         'September',
    //         'Oktober',
    //         'November',
    //         'Desember',
    //     );
    //     $date = date($date_format, $timestamp);
    //     $date = preg_replace($pattern, $replace, $date);
    //     $date = "{$date}";
    //     return $date;
    // }
    // public function indonesian_date_new($timestamp = '', $date_format = 'l, j F Y, H:i')
    // {
    //     if (trim($timestamp) == '') {
    //         $timestamp = time();
    //     } elseif (!ctype_digit($timestamp)) {
    //         $timestamp = strtotime($timestamp);
    //     }
    //     # remove S (st,nd,rd,th) there are no such things in indonesia :p
    //     $date_format = preg_replace("/S/", "", $date_format);
    //     $pattern = array(
    //         '/Mon[^day]/',
    //         '/Tue[^sday]/',
    //         '/Wed[^nesday]/',
    //         '/Thu[^rsday]/',
    //         '/Fri[^day]/',
    //         '/Sat[^urday]/',
    //         '/Sun[^day]/',
    //         '/Monday/',
    //         '/Tuesday/',
    //         '/Wednesday/',
    //         '/Thursday/',
    //         '/Friday/',
    //         '/Saturday/',
    //         '/Sunday/',
    //         '/Jan[^uary]/',
    //         '/Feb[^ruary]/',
    //         '/Mar[^ch]/',
    //         '/Apr[^il]/',
    //         '/May/',
    //         '/Jun[^e]/',
    //         '/Jul[^y]/',
    //         '/Aug[^ust]/',
    //         '/Sep[^tember]/',
    //         '/Oct[^ober]/',
    //         '/Nov[^ember]/',
    //         '/Dec[^ember]/',
    //         '/January/',
    //         '/February/',
    //         '/March/',
    //         '/April/',
    //         '/June/',
    //         '/July/',
    //         '/August/',
    //         '/September/',
    //         '/October/',
    //         '/November/',
    //         '/December/',
    //     );
    //     $replace = array(
    //         'Sen',
    //         'Sel',
    //         'Rab',
    //         'Kam',
    //         'Jum',
    //         'Sab',
    //         'Min',
    //         'Senin',
    //         'Selasa',
    //         'Rabu',
    //         'Kamis',
    //         'Jumat',
    //         'Sabtu',
    //         'Minggu',
    //         'Jan',
    //         'Feb',
    //         'Mar',
    //         'Apr',
    //         'Mei',
    //         'Jun',
    //         'Jul',
    //         'Ags',
    //         'Sep',
    //         'Okt',
    //         'Nov',
    //         'Des',
    //         'Januari',
    //         'Februari',
    //         'Maret',
    //         'April',
    //         'Juni',
    //         'Juli',
    //         'Agustus',
    //         'September',
    //         'Oktober',
    //         'November',
    //         'Desember',
    //     );
    //     $date = date($date_format, $timestamp);
    //     $date = preg_replace($pattern, $replace, $date);
    //     $date = "{$date}";
    //     return $date;
    // }

    // public function waktu_lalu($timestamp)
    // {
    //     date_default_timezone_set('Asia/Jakarta');
    //     $selisih = time() - strtotime($timestamp);
    //     $detik = $selisih;
    //     $menit = round($selisih / 60);
    //     $jam = round($selisih / 3600);
    //     $hari = round($selisih / 86400);
    //     $minggu = round($selisih / 604800);
    //     $bulan = round($selisih / 2419200);
    //     $tahun = round($selisih / 29030400);
    //     if ($detik <= 60) {
    //         $waktu = $detik . ' detik yang lalu';
    //     } else if ($menit <= 60) {
    //         $waktu = $menit . ' menit yang lalu';
    //     } else if ($jam <= 24) {
    //         $waktu = $jam . ' jam yang lalu';
    //     } else if ($hari <= 7) {
    //         $waktu = $hari . ' hari yang lalu';
    //     } else if ($minggu <= 4) {
    //         $waktu = $minggu . ' minggu yang lalu';
    //     } else if ($bulan <= 12) {
    //         $waktu = $bulan . ' bulan yang lalu';
    //     } else {
    //         $waktu = $tahun . ' tahun yang lalu';
    //     }
    //     return $waktu;
    // }

    // public function cdn_images($inid)
    // {
    //     $CI = &get_instance();
    //     $arr_img = array();
    //     $query = $CI->db->select('*')->from('arsip_images')->where("arimId", $inid)->get()->result();
    //     foreach ($query as $r) {
    //         $full = $r->arimId;
    //         $split_id = str_split($r->arimId);
    //         $path_folder_image = implode('/', $split_id);
    //         $r->arimTitle = str_replace("-", "", $r->arimTitle);
    //         $r->arimId = url_title($r->arimTitle, "_", true);

    //         $arr_img = array(
    //             'caption' => $r->arimCaption,
    //             'img1' => URL_IMG . "{$path_folder_image}/{$inid}.{$r->arimFileType}",
    //             'img2' => URL_IMG . "{$path_folder_image}/{$inid}_263x180.{$r->arimFileType}",
    //             'img3' => URL_IMG . "{$path_folder_image}/{$inid}_300x206.{$r->arimFileType}",
    //             'img4' => URL_IMG . "{$path_folder_image}/{$inid}_512x351.{$r->arimFileType}",
    //             'img5' => URL_IMG . "{$path_folder_image}/{$inid}_683x468.{$r->arimFileType}",
    //             'img6' => URL_IMG . "{$path_folder_image}/{$inid}_840x576.{$r->arimFileType}",
    //             'img7' => URL_IMG . "{$path_folder_image}/{$full}.{$r->arimFileType}"
    //         );
    //     }

    //     return $arr_img;
    // }
    
    


    // public function cdn_images_one($inid)
    // {
    //     $CI = &get_instance();
    //     $arr_img = array();
    //     $query = $CI->db->select('*')->from('arsip_images')->where("arimId", $inid)->get()->result();
    //     foreach ($query as $r) {
    //         $full = $r->arimId;
    //         $split_id = str_split($r->arimId);
    //         $path_folder_image = implode('/', $split_id);
    //         $r->arimTitle = str_replace("-", "", $r->arimTitle);
    //         $r->arimId = url_title($r->arimTitle, "_", true);

    //         $arr_img = array(
    //             'caption' => $r->arimCaption,
    //             'img1' => URL_IMG . "{$path_folder_image}/{$inid}.{$r->arimFileType}",
    //             // 'img1' => URL_IMG . "{$path_folder_image}/{$inid}.{$r->arimFileType}?" . rand(),
    //             // 'img2' => URL_IMG . "{$path_folder_image}/{$r->arimId}_263x180.{$r->arimFileType}",
    //             // 'img3' => URL_IMG . "{$path_folder_image}/{$r->arimId}_300x206.{$r->arimFileType}",
    //             // 'img4' => URL_IMG . "{$path_folder_image}/{$r->arimId}_512x351.{$r->arimFileType}",
    //             // 'img5' => URL_IMG . "{$path_folder_image}/{$r->arimId}_683x468.{$r->arimFileType}",
    //             // 'img6' => URL_IMG . "{$path_folder_image}/{$r->arimId}_840x576.{$r->arimFileType}",
    //             // 'img7' => URL_IMG . "{$path_folder_image}/{$full}.{$r->arimFileType}"
    //         );
    //     }

    //     return $arr_img['img1'];
    // }

    // public function getYoutubeId($tmp)
    // {
	// 	preg_match('/src="([^"]+)"/', $tmp, $match);
	// 	$url = '';
	// 	if(count($match) > 1){
	// 		$url = $match[1];
	// 	}
        

    //     parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
    //     if ($my_array_of_vars) {
    //         if (empty($my_array_of_vars['v'])) {
    //             return '';
    //         } else {
    //             return $my_array_of_vars['v'];
    //         }
    //     } else {
	// 		$last_word = '';
    //         preg_match("/[^\/]+$/", $url, $matches);
    //         if (!empty($matches)) {
    //             $last_word = $matches[0];
    //         }
    //         // print_r($matches);die();
    //         return $last_word;
    //     }
    // }



    // public function cdn_images_randomcache($inid)
    // {
    //     $CI = &get_instance();
    //     $arr_img = array();
    //     $query = $CI->db->select('*')->from('arsip_images')->where("arimId", $inid)->get()->result();
    //     foreach ($query as $r) {
    //         $full = $r->arimId;
    //         $split_id = str_split($r->arimId);
    //         $path_folder_image = implode('/', $split_id);
    //         $r->arimTitle = str_replace("-", "", $r->arimTitle);
    //         $r->arimId = url_title($r->arimTitle, "_", true);

    //         $arr_img = array(
    //             'caption' => $r->arimCaption,
    //             'img1' => URL_IMG . "{$path_folder_image}/{$inid}.{$r->arimFileType}?".rand(),
    //             'img2' => URL_IMG . "{$path_folder_image}/{$inid}_263x180.{$r->arimFileType}?".rand(),
    //             'img3' => URL_IMG . "{$path_folder_image}/{$inid}_300x206.{$r->arimFileType}?".rand(),
    //             'img4' => URL_IMG . "{$path_folder_image}/{$inid}_512x351.{$r->arimFileType}?".rand(),
    //             'img5' => URL_IMG . "{$path_folder_image}/{$inid}_683x468.{$r->arimFileType}?".rand(),
    //             'img6' => URL_IMG . "{$path_folder_image}/{$inid}_840x576.{$r->arimFileType}?".rand(),
    //             'img7' => URL_IMG . "{$path_folder_image}/{$full}.{$r->arimFileType}"
    //         );
    //     }

    //     return $arr_img;
    // }
    
    
    //  public function cdn_videos($inid)
    // {
    //     $CI = &get_instance();
    //     $arr_img = array();
    //     $query = $CI->db->select('*')->from('arsip_images')->where("arimId", $inid)->get()->result();
    //     foreach ($query as $r) {
    //         $full = $r->arimId;
    //         $split_id = str_split($r->arimId);
    //         $path_folder_image = implode('/', $split_id);
    //         $r->arimTitle = str_replace("-", "", $r->arimTitle);
    //         $r->arimId = url_title($r->arimTitle, "_", true);
            
    //         $templates = '<div style="position:relative;padding-bottom:56.25%;height:0;overflow:hidden;">
    //         <iframe 
    //             style="width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden" 
    //             frameborder="0" 
    //             type="text/html" 
    //             src='.URL_IMG.$path_folder_image.'/'.$inid.'.'.$r->arimFileType.'
    //             width="100%" 
    //             height="100%" 
    //             allow="autoplay" 
    //             allowfullscreen>
    //         </iframe>
    //         </div>';

    //         $arr_img = array(
    //             'caption' => $r->arimCaption,
    //             'img1' =>$templates// URL_IMG . "{$path_folder_image}/{$inid}.{$r->arimFileType}"
    //         );
    //     }

    //     return $arr_img;
    // }

    // public function get_video_server($id)
    // {
    //     $CI = &get_instance();
    //     $query = $CI->db->select('*')->from('arsip_images')->where("arimId", $id)->get()->result();
    //     $video='';
    //     foreach ($query as $r) {
    //         $split_id = str_split($r->arimId);
	// 		$path_folder_image = implode('/', $split_id);
    //         // $video = "../public_html/assets/uploads/"."{$path_folder_image}/{$r->arimId}.{$r->arimFileType}";
    //         // $video="https://tvradio.polri.go.id/api/downloadapi/download?id=".$id;
    //         $video = "https://tvradio.polri.go.id/assets/uploads/"."{$path_folder_image}/{$r->arimId}.{$r->arimFileType}";
    //      }
    //     return $video;
    // }
    
    // public function indonesian_date_and_month($timestamp = '', $date_format = 'j F', $suffix = 'WIB')
    // {
    //     if (trim($timestamp) == '') {
    //         $timestamp = time();
    //     } elseif (!ctype_digit($timestamp)) {
    //         $timestamp = strtotime($timestamp);
    //     }
    //     # remove S (st,nd,rd,th) there are no such things in indonesia :p
    //     $date_format = preg_replace("/S/", "", $date_format);
    //     $pattern = array(
    //         '/Mon[^day]/',
    //         '/Tue[^sday]/',
    //         '/Wed[^nesday]/',
    //         '/Thu[^rsday]/',
    //         '/Fri[^day]/',
    //         '/Sat[^urday]/',
    //         '/Sun[^day]/',
    //         '/Monday/',
    //         '/Tuesday/',
    //         '/Wednesday/',
    //         '/Thursday/',
    //         '/Friday/',
    //         '/Saturday/',
    //         '/Sunday/',
    //         '/Jan[^uary]/',
    //         '/Feb[^ruary]/',
    //         '/Mar[^ch]/',
    //         '/Apr[^il]/',
    //         '/May/',
    //         '/Jun[^e]/',
    //         '/Jul[^y]/',
    //         '/Aug[^ust]/',
    //         '/Sep[^tember]/',
    //         '/Oct[^ober]/',
    //         '/Nov[^ember]/',
    //         '/Dec[^ember]/',
    //         '/January/',
    //         '/February/',
    //         '/March/',
    //         '/April/',
    //         '/June/',
    //         '/July/',
    //         '/August/',
    //         '/September/',
    //         '/October/',
    //         '/November/',
    //         '/December/',
    //     );
    //     $replace = array(
    //         'Sen',
    //         'Sel',
    //         'Rab',
    //         'Kam',
    //         'Jum',
    //         'Sab',
    //         'Min',
    //         'Senin',
    //         'Selasa',
    //         'Rabu',
    //         'Kamis',
    //         'Jumat',
    //         'Sabtu',
    //         'Minggu',
    //         'Jan',
    //         'Feb',
    //         'Mar',
    //         'Apr',
    //         'Mei',
    //         'Jun',
    //         'Jul',
    //         'Ags',
    //         'Sep',
    //         'Okt',
    //         'Nov',
    //         'Des',
    //         'Januari',
    //         'Februari',
    //         'Maret',
    //         'April',
    //         'Juni',
    //         'Juli',
    //         'Agustus',
    //         'September',
    //         'Oktober',
    //         'November',
    //         'Desember',
    //     );
    //     $date = date($date_format, $timestamp);
    //     $date = preg_replace($pattern, $replace, $date);
    //     $date = "{$date}";
    //     return $date;
    // }
    
    

}
