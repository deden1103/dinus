<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Outlet extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Outlet_Model']);
    }

    public function index_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());
        $query = $this->Outlet_Model->add_outlet($post, $this->jwtData->id);
        $response = [
            'success' => true,
            'data' => $query,
        ];
        $this->response($response, 200);
    }

    public function my_outlet_get()
    {
        $id = $this->jwtData->id; //intval($this->get('id'));
        $d=security_single_post($this->get('d'));
        $m=security_single_post($this->get('m'));
        $y=security_single_post($this->get('y'));

        $page = 1;
        $searchtext = '';
        if ($this->get('p')) {
            $page = intval($this->get('p'));
        }

        if ($this->get('s')) {
            $searchtext = security_single_post($this->get('s'));
        }

        $rows = 10;
        $offset = ($page - 1) * $rows;

        $query = $this->Outlet_Model->mylist($id, $d, $m, $y);
        $response = [
            'success' => true,
            'data' => $query,
        ];
        $this->response($response, 200);
    }

    public function all_outlet_get()
    {
        $id = $this->jwtData->id;   

        $query = $this->Outlet_Model->all_list($id);
        $response = [
            'success' => true,
            'data' => $query,
        ];
        $this->response($response, 200);
    }

    public function detail_get()
    {
        $id = $this->get('id');
        $query = $this->Outlet_Model->detail_outlet($id);
        $response = [
            'success' => true,
            'data' => $query,
        ];
        $this->response($response, 200);
    }


    public function update_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());
        $query = $this->Outlet_Model->update_outlet($post, $this->jwtData->id);
        $response = [
            'success' => true,
            'data' => $query,
        ];
        $this->response($response, 200);
    }

}
