<?php
defined('BASEPATH') or exit('No direct script access allowed');

class City extends API_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['City_Model']);
    }

    public function list_get()
    {
      
        $page = 1;
        $searchtext = '';
        if ($this->get('p')) {
            $page = intval($this->get('p'));
        }

        if ($this->get('s')) {
            $searchtext = security_single_post($this->get('s'));
        }

        $rows = 10;
        $offset = ($page - 1) * $rows;

        $query = $this->City_Model->list($rows,$offset,$searchtext);
        $response = [
            'success' => true,
            'data' => $query,
        ];
        $this->response($response, 200);
    }

    

}
