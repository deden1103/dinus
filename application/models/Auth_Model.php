<?php
class Auth_Model extends CI_Model
{
    function login($usercode, $password)
    {
        $query = $this->db
            ->select('*')
            ->from('user')
            ->where('userPassword', $password)
            // ->where("status",1)
            ->where('userUsername', $usercode)
            ->get();
        //echo $this->db->last_query();
        return $query->row();
    }

    public function register($data)
    {
        $this->db->insert('anggota', $data);
        if ($this->db->affected_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    function logingoogle($email)
    {
        $where = [
            'emailAnggota' => $email,
        ];
        $this->db->where($where);
        $query = $this->db->get('anggota');
        return $query->row();
    }

    function loginsso($emailorphone)
    {
        $query = $this->db
            ->select('*')
            ->from('user')
            ->group_start()
            ->where('PEG_NIP_BARU', $emailorphone)
            ->or_where('userEmail', $emailorphone)
            ->group_end()
            ->get();
        //echo $this->db->last_query();
        return $query->row();
    }
}
