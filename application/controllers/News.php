<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class News extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['News_Model']);
    }

    public function index_get()
    {
        $page = 1;
        $searchtext = '';
        if ($this->get('p')) {
            $page = intval($this->get('p'));
        }

        if ($this->get('s')) {
            $searchtext = security_single_post($this->get('s'));
        }

        $rows = 10;
        $offset = ($page - 1) * $rows;
        $listNews = $this->News_Model->list_news($rows, $offset, $searchtext);
        $response = [
            'success' => true,
            'page' => $page,
            'next_page' => $page + 1,
            'data' => $listNews,
        ];
        $this->response($response, 200);
    }
}
