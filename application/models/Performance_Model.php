<?php
class Performance_Model extends CI_Model
{
    function detail($userid)
    {
        $sql = 'SELECT * FROM itenary';
        $sql .= ' LEFT JOIN user ON itenary.itenUserId = user.userId';
        $sql .= ' LEFT JOIN outlet ON itenary.itenOutletId = outlet.outId';
        $sql .= " WHERE itenUserId = {$userid}";
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return $query;
        } else {
            return [];
        }
    }

    function target_visit($userid, $bulan, $tahun)
    {
        $sql = 'SELECT count(itenId) as total_itenary FROM itenary';
        $sql .= " WHERE itenUserId = {$userid}";
        $sql .= " AND MONTH(itenDate) = '{$bulan}'";
        $sql .= " AND YEAR(itenDate) = '{$tahun}'";
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return intval($query[0]['total_itenary']);
        } else {
            return 0;
        }
    }

    function data_visit_id($userid)
    {
        $sql = 'SELECT vstItenId FROM visit';
        $sql .= " WHERE vstUserId = {$userid}";
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            $data = array_column($query, 'vstItenId');
            $in = '(' . implode(',', $data) . ')';
            return $in;
        } else {
            return 0;
        }
    }

    function count_sudah($userid, $bulan, $tahun)
    {
        $sql = 'SELECT count(vstItenId) as jumlah_dikunjungi FROM visit';
        $sql .= " WHERE vstUserId = {$userid}";
        $sql .= ' AND vstCheckIn IS NOT NULL';
        $sql .= ' AND vstCheckOut IS NOT NULL';
        $sql .= " AND MONTH(vstDate) = '{$bulan}'";
        $sql .= " AND YEAR(vstDate) = '{$tahun}'";
        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return intval($query[0]['jumlah_dikunjungi']);
        } else {
            return 0;
        }

        // $sql = 'SELECT * FROM visit';
        // $sql .= " WHERE vstUserId = {$userid}";
        // // $sql = 'SELECT * FROM visit WHERE vstItenId NOT IN ' . $in;
        // // $sql = 'SELECT vstItenId as sudah FROM visit';
        // // $sql .= " WHERE vstUserId = {$userid}";
        // $query = $this->db->query($sql)->result_array($sql);
        // if ($query) {
        //     return $query;
        // } else {
        //     return 0;
        // }
    }

    function count_photo($userid, $type)
    {
        $sql = 'SELECT count(visId) as total FROM visibility';

        if ($type == 'ok') {
            $sql .= " WHERE visUserId = {$userid}";
            $sql .= ' AND visPhoto IS NOT NULL';
        } elseif ($type == 'notok') {
            $sql .= " WHERE visUserId = {$userid}";
            $sql .= ' AND visPhoto IS NULL';
        } else {
            $sql .= " WHERE visUserId = {$userid}";
        }

        $query = $this->db->query($sql)->result_array($sql);
        if ($query) {
            return intval($query[0]['total']);
        } else {
            return 0;
        }
    }
}
