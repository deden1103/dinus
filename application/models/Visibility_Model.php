<?php
class Visibility_Model extends CI_Model
{
    function add($data)
    {
        // print_r($_FILES['photo']);
        $this->visUserId = $data['user_id']; // please read the below note
        $this->visOutletId = $data['outlet_id'];
        $this->visActId = $data['act_id'];
        $this->visDate = date("Y-m-d");
        // $this->visPhoto = $data['photo'];
        $this->visUserSaved = $data['user_saved'];
        
        $this->db->insert('visibility',$this);

        $id = $this->db->insert_id();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $res = [
                "id" => null,
                "status" => false,
                "message" => "Data failed saved",
            ];
        } else {
            $this->db->trans_commit();
            $res = [
                "id" => $id,
                "status" => true,
                "message" => "Data has been saved successfully!",
            ];
        }
        return $res;
    }

    function add_photo($data)
    {
        $this->db->insert('visibility_photo', [
            'visibilityId' => $data['id'],
            'visibilityPhoto' => $data['file_name'],
            'visibilityStatus' => 1,
        ]);

        $insertid = $this->db->insert_id();

        $id = $this->db->insert_id();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            $res = [
                "id" => null,
                "status" => false,
                "message" => "Data failed saved",
            ];
        } else {
            $this->db->trans_commit();
            $res = [
                "id" => $id,
                "status" => true,
                "message" => "Data has been saved successfully!",
            ];
        }
        return $res;
    }
}
