<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Auth extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Auth_Model']);
    }
    public function index_post()
    {
        $post = $this->post();
        $this->form_validation->set_data($this->post());

        $usercode = $post['username'];
        $password = base64_encode(hash('sha256', sha1($post['password'])));

        $this->form_validation->set_rules('username', 'username', 'trim|required', [
            'required' => 'email/phone harus diisi',
        ]);

        $this->form_validation->set_rules(
            'password',
            'Password',
            'trim|required',
            [
                'required' => 'Kata Sandi harus diisi',
            ]
        );

        if ($this->form_validation->run() == true) {
            $userData = $this->Auth_Model->login($usercode, $password);
            if ($userData) {
                $response = [
                    'data' => $userData,
                    'message' => 'Login Berhasil',
                    'success' => true,
                ];

                $token['id'] = $userData->userId;
                $token['userLevelId'] = $userData->userLevelId;
                $token['userCode'] = $userData->userCode;
                $token['userUsername'] = $userData->userUsername;
                $token['userAreaName'] = $userData->userAreaName;
                $token['userIdDevice'] = $userData->userIdDevice;
                $token['userStatus'] = $userData->userStatus;
                $token['email'] = $userData->userEmail;
                $token['logged'] = true;
                $token['iat'] = time();
                $token['exp'] = time() + 86400; //86400
                $jwt_token = JWT::encode($token, secretKey());
                $jwt = [
                    'token' => $jwt_token,
                ];
                $response = array_merge($response, $jwt);
            } else {
                $response = [
                    'message' => 'User ID atau Password tidak dikenal !',
                    'success' => false,
                    'pass' => $password,
                ];
            }
        } else {
            $response = [
                'errors' => $this->form_validation->error_array(),
                'success' => false,
            ];
        }

        $this->response($response, 200);
    }
}
/*

"userId": "3",
"userAreaId": "1",
"userLevelId": "1",
"userCode": "123321",
"userUsername": "admin",
"userPassword": "25d55ad283aa400af464c76d713c07ad",
"userAreaName": "1",
"userIdDevice": "",
"userPhone": "1231231",
"userEmail": "admin@kabarbumn.com",
"userAddress": "Jakarta",
"userCity": "dki jakarta22",
"userLatitude": "",
"userLongitude": "",
"userPhoto": "fe7e9e5a703f22662c82f72f12b98cca.png",
"userStatus": "1",
"userSaved": "2022-02-22 13:07:17",
"userUpdated": "2022-03-22 14:31:23"
5e6896ee19506b768e5c2e1edb47f9e440de0e8103d4b65b90a7657e14ea1afe

*/
