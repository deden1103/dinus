<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class About extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_global');

    }

    public function index_get()
    {
        $dataDetail = array();
        $this->db->select('*');
        $this->db->from('about');
        $query = $this->db->get()->row();

        $response = array(

            'detail' => $query,
            'success' => true,
        );
        $this->response($response, 200);
    }

    public function help_get()
    {
        $dataDetail = array();
        $this->db->select('*');
        $this->db->from('help');
        $query = $this->db->get()->result();
        foreach ($query as $r) {
            $dataDetail[] = array(
                'helpId' => $r->helpId,
                'helpTanya' => $r->helpTanya,
                'helpJawab' => $r->helpJawab,
            );
        }
        $response = array(

            'detail' => $dataDetail,
            'success' => true,
        );
        $this->response($response, 200);
    }

    public function edit_submit_post()
    {

        $post = $this->post();
        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('desc', 'desc', 'trim|required');
        $this->form_validation->set_message('required', '{field} harus diisi');

        

        if ($this->form_validation->run() == true) {

            $data = array(
                'aboutDesc' => @$post['desc'],
            );
            $where = array(
                'aboutId' => 1,
            );
            $save = $this->M_global->update('about', $data, $where);

            print_r($data);die();

            $response = array(
                // 'data' => $post,
                'message' => 'Update data about '.rand(),
                'success' => true,
            );
        }else{
            $response = array(
                'data' => $post,
                'message' => 'gagal',
                'errors' => $this->form_validation->error_array(),
                'success' => true,
            );
        }

        
        $this->response($response, 200);
    }

}
